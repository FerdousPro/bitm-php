<!--
The array_unshift() function inserts new elements to an array.
The new array values will be inserted in the beginning of the array.

Tip: We can add one value, or as many as we like.
Note: Numeric keys will start at 0 and increase by 1. String keys will remain the same.

Syntax => array_unshift(array,value1,value2,value3...) 
-->
<?php
$a=array("a"=>"Tamim", "b"=>"Iqbal");
array_unshift($a, "Sakib" );
print_r($a);
?>
<!--
output:

Array ( [0] => Sakib [a] => Tamim [b] => Iqbal ) 
-->