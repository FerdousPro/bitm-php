<!--
The shuffle() function randomizes the order of the elements in the array.
Note: This function assigns new keys for the elements in the array. Existing keys will be removed

Syntax => shuffle(array) 
-->
<?php
$a = array("google", "bing", "yahoo");
shuffle($a);
print_r($a);
?>
<!--
output:

Array ( [0] => bing [1] => google [2] => yahoo ) 
-->