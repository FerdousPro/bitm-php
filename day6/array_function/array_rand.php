<!--
The array_rand() function returns a random key from an array, or it returns
an array of random keys if you specify that the function should return more than one key.

Syntax =>  array_rand(array,number) 
-->
<?php
// Return an array of random keys
$a=array("red","green","blue","yellow","brown");
$random_keys=array_rand($a,3);
echo $a[$random_keys[0]]."<br>";
echo $a[$random_keys[1]]."<br>";
echo $a[$random_keys[2]];
echo "<br/>";

// Return a random key from an array
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
print_r(array_rand($a,1));
echo "<br/>";

//Return an array of random string keys
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
print_r(array_rand($a,2));
?> 

<!--
output:

output will be tree random result
-->