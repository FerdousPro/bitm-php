<!--
The array_pad() function inserts a specified number of elements, with a specified value, to an array.

Tip: If we assign a negative size parameter, the function will insert new elements BEFORE the original elements
Note: This function will not delete any elements if the size parameter is less than the size of the original array.

Syntax =>  array_merge(array1,array2,array3...)
-->

<?php
$a=array("red","green");
print_r(array_pad($a,5,"blue"));//return 5 elements and insert a value of "blue" to the new elements in the array
?>

<!--
output:

Array ( [0] => red [1] => green [2] => blue [3] => blue [4] => blue )
-->