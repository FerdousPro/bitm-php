<!--
This function is used to replace each array element equal to the given value with a new value.

Syntax =>  array_replace(array1,array2,array3...) 
-->
<?php
$a=array('Ferdous', 'Iqbal');
$b=array("Google", "Bing");
print_r(array_replace($a, $b));
?>
<!--
output:

Array ( [0] => Google [1] => Bing ) 
-->