<!--
The array_sum() function returns the sum of all the values in the array.

Syntax =>  array_sum(array) 
-->

<?php
$data=array(12, 62, 69);
print_r(array_sum($data));
?>

<!--
output:

143
-->