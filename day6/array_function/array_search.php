<!--
The array_search() function search an array for a value and returns the key.

Syntax => array_search(value,array,strict) 
-->

<?php
$himu = array("a"=>"red", "b"=>"green", "c"=> "blue");
print_r(array_search("red", $himu));
?>

<!--
output:

a
-->