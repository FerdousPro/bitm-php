<!--
The array_pop() function deletes the last element of an array.

Syntax => array_pop(array) 
-->
<?php
$a=array("red","green","blue");
array_pop($a);
print_r($a);
?> 
<!--
output:

Array ( [0] => red [1] => green ) 
-->