<!--
The array_push() function inserts one or more elements to the end of an array.
Note: Even if the array has string keys, the added elements will always have numeric keys

Syntax =>  array_push(array,value1,value2...) 
-->
<?php
$a = array('Ferdous', 'Iqbal');
array_push($a, 'Rajib');
print_r($a);
?>
<!--
output:

Array ( [0] => Ferdous [1] => Iqbal [2] => Rajib ) 
-->