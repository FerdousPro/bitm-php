<!--
The array_merge() function merges one or more arrays into one array.

Syntax =>  array_merge(array1,array2,array3...)
-->

<?php
$a1=array("red","green");
$a2=array("blue","yellow");
print_r(array_merge($a1,$a2));
?>

<!--
output:

Array ( [0] => red [1] => green [2] => blue [3] => yellow )
-->