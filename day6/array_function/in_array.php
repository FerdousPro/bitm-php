<!--
The in_array() function searches an array for a specific value.
Note: If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.

Syntax =>  in_array(search,array,type) 
-->
<?php
$a=array("a"=>"Tamim", "b"=>"Iqbal", "c"=>"Mostafiz");
if (in_array("Mostafiz", $a)) {
	echo "Match found";
}
else {
	echo "Match not found";
}

?>
<!--
output:

Match found
-->