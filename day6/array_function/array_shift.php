<!--
The array_shift() function removes the first element from an array, and returns the value of the removed element.

Syntax => array_shift(array) 
-->
<?php
$a=array("a"=>"Tamim", "b"=>"Iqbal");
array_shift($a);
print_r($a);
?>
<!--
output:

Array ( [b] => Iqbal ) 
-->