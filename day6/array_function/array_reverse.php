<!--
The array_reverse() function returns an array in the reverse order.

Syntax =>  array_reverse(array,preserve)  
-->

<?php
$himu = array("a"=>"red", "b"=>"green", "c"=> "blue");
print_r(array_reverse($himu));
?>

<!--
output:

Array ( [c] => blue [b] => green [a] => red ) 
-->