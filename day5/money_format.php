<!-- 
The money_format() function returns a string formatted as a currency string.

 Syntax =>    money_format(string,number) 

<?php
$number = 1234.56;
setlocale(LC_MONETARY,"en_US");
echo money_format("The price is %i", $number);
?>

<!--Problem-->