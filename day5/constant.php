<!-- 
To create a constant, use the define() function.

Syntax =>  define(name, value, case-insensitive)
-->
<?php
// case-sensitive constant name
define("GREETING", "Welcome to my website!");
echo GREETING . "<br />";

// case-insensitive constant name
define("MEETING", "Welcome here!", true);
echo meeting . "<br />";

//__NAMESPACE__
// namespace anyname;
// echo __NAMESPACE__ . "<br>";

//__LINE__

echo __LINE__ . "<br>";

//__FILE__

echo __FILE__ . "<br>";

//__DIR__

echo __DIR__ . "<br>";

function bitm() {
    echo "My name is BITM" . "<br>";
    echo __FUNCTION__ . "<br>";
}

bitm();

echo !0;
?>

<!--
output

Welcome to my website!
Welcome here!
21
C:\xampp\htdocs\bitm-php\day5\constant.php
C:\xampp\htdocs\bitm-php\day5
My name is BITM
bitm
1
-->