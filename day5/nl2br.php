<!--
The nl2br()function is used to insert HTML line breaks ('<br />') before all newlines (\n) in a string.

Syntax =>    nl2br(input_string)
-->

<?php
echo nl2br("One line.\nAnother line.");
?>
<!--
output:

One line.
Another line.
-->
