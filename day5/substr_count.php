<!--
The strtolower() function converts a string to uppercase.

Syntax => substr_count(string,substring,start,length)
-->

<?php
echo substr_count("Hello world. The world is nice","world");
echo "<br />";
$str = "abcabcab";
echo substr_count($str,"abcab"); // This function does not count overlapped substrings
?>
<!--
output:

2
1
-->