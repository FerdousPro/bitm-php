<!--
The str_repeat() function repeats a string a specified number of times.

Syntax =>  str_repeat(string,repeat)
-->

<?php
echo str_repeat("Ferdous ",13);
?>
<!--
output:

Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous Ferdous
-->