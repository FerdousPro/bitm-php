<!--
The strtolower() function converts a string to lowercase.

Syntax => strtolower(string)
-->

<?php
$data = "BanglaDesh";
echo strtolower($data);
?>
<!--
output:

bangladesh
-->