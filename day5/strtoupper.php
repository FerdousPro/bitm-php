<!--
The strtolower() function converts a string to uppercase.

Syntax => strtoupper(string)
-->

<?php
$data = "Bangladesh";
echo strtoupper($data);
?>
<!--
output:

BANGLADESH
-->