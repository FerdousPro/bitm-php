<!-- 
The htmlentities() function is used to convert all applicable characters to HTML entities.

 Syntax =>  htmlentities(string,flags,character-set,double_encode) only string is required  
-->

<?php
$str = "<© W3Sçh°°¦§>";
echo htmlentities($str);
?>

<!--
output:

<© W3Sçh°°¦§>
-->