<!--
The strlen() function returns the length of a string.

Syntax =>  strlen(string)
-->

<?php
$data = "Bangladesh";
echo strlen($data);
?>
<!--
output:

10
-->