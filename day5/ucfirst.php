<!--
The ucfirst() function converts the first character of a string to uppercase.

Syntax =>  ucfirst(string)
-->

<?php
echo ucfirst("hello world!");
?>

<!--
output:

Hello world!
-->