<!--
The strip_tags() function strips a string from HTML, XML, and PHP tags.

Syntax => strip_tags(string,allow)
-->

<?php
$data = "Hello <b>world!</b>";
echo strip_tags($data);
?>
<!--
output:

Hello world!
-->