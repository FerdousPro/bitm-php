<!--
The substr_replace() function replaces a part of a string with another string.

Syntax => substr_replace(string,replacement,start,length)
-->
<?php
echo substr_replace("Welcome Bangladesh", "to", 7) . "<br />";
echo substr_replace("Hello","world",0); // 0 will start replacing at the first character in the string
?>

<!--
output:

Welcometo
world
-->
