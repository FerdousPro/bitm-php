<!--
The addslashes() function returns a string with backslashes in front of predefined characters.

 addslashes(string) 
-->

<?php
$str = addslashes('What does "yolo" mean?');
echo($str);
?>

<!--
output:

What does \"yolo\" mean?
-->