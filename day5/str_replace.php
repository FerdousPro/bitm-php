<!--
The str_replace() function replaces some characters with some other characters in a string.

Syntax =>  str_replace(find,replace,string,count)
-->

<?php
echo str_replace("world","Ferdous","Hello world! Hi"); //Replace the characters "world" in the string "Hello world!" with "Ferdous"
?>
<!--
output:

Hello Ferdous! Hi
-->