<!--
The str_split() function splits a string into an array.

Syntax =>  str_split(string,length)
-->

<?php
$data ="Bangladesh";
print_r(str_split($data));
?>
<!--
output:

Array ( [0] => B [1] => a [2] => n [3] => g [4] => l [5] => a [6] => d [7] => e [8] => s [9] => h )
-->