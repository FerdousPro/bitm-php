<!-- 
The trim() function removes whitespace and other predefined characters from both sides of a string.

Syntax =>   trim(string,char_list)
-->

<?php
$str = "Hello World!";
echo $str . "<br>";
echo trim($str,"Hed!"); // it can only trim matched characters in sequence from first and last
?>

<!--
output:

Hello World!
llo Worl
-->
