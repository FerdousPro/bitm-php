<!-- 
Break a string into an array

Syntax => explode(separator,string,limit)
-->
<?php
$str = "Hello world. It's a beautiful day.";
echo "<pre>";
print_r (explode(" ",$str));
echo "</pre>";
?>

<!--
output:

Array
(
    [0] => Hello
    [1] => world.
    [2] => It's
    [3] => a
    [4] => beautiful
    [5] => day.
)
-->
