<!-- 
The implode() function returns a string from the elements of an array.

Syntax =>  implode(separator,array)
-->

<?php
$arr = array('Hello','World!','Beautiful','Day!');
echo implode(" ",$arr); // anything between the quotes will be placed after every array
?>

<!--
output:

Hello World! Beautiful Day!
-->


