<!--
The str_pad() function pads a string to a new length.

Syntax =>   str_pad(string,length,pad_string,pad_type)
-->

<?php
$str = "Hello World";
echo str_pad($str,20,".");
?>
<!--
output:

Hello World.........
-->