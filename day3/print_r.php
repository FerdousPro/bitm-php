<!--The print_r() function is used to print human-readable information about a variable.
Array can't be ECHO(unless we use specific element)so we have to use print_r

Syntax => print_r(variable_name)
-->


<pre>
    <?php
    $ferdous = array('0' => 'Love', '1' => 'Programming', '2' => array('a', 'b', 'c'));
    print_r($ferdous);      // var_dump display more information, print_r is not as organized as var_dump
    ?>
</pre>