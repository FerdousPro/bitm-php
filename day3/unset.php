<!--The unset() function destroys a given variable.

Syntax => unset (variable1, variable2.... )
-->

<?php

$bitm = "12";
var_dump(isset($bitm)); // display the type and value of the variable

unset($bitm);
var_dump(isset($bitm));

?>