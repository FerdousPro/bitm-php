<!--The isset () function is used to check whether a variable is set or not.
If a variable is already unset with unset() function, it will no longer be set.
The isset() function return false if testing variable contains a NULL value

Syntax => isset(variable1, variable2......)
-->

<?php
$data = null;

if (isset($data)){
    echo "The variable is null";
}
else {
    echo "The variable is not null";
}
?>


