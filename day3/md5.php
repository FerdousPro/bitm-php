<!-- 
The md5() function calculates the MD5 hash of a string.

The md5() function uses the RSA Data Security, Inc. MD5 Message-Digest Algorithm.

From RFC 1321 - The MD5 Message-Digest Algorithm: "The MD5 message-digest algorithm takes as input a message of arbitrary length and produces as output a 128-bit "fingerprint" or "message digest" of the input. The MD5 algorithm is intended for digital signature applications, where a large file must be "compressed" in a secure manner before being encrypted with a private (secret) key under a public-key cryptosystem such as RSA." 

syntax => empty(var_name) 
 -->
<?php
$str="Hello";
echo md5($str);  //Calculate the MD5 hash of the string "Hello"
?> 

<!-- 
output:

8b1a9953c4611296a827abf8c47804d7
-->
