<!-- 
The empty() function is used to check whether a variable is empty or not. 

syntax => empty(var_name) 
 -->
<?php
if (empty($_POST['email'])) {
    echo "Please enter your email address";
}
?> 
