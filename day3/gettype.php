<!--
The gettype() function is used to get the type of a variable.

Syntax => gettype(variable_name)
-->

<?php

echo gettype(62) . "<br/>";
echo gettype(false) . "<br/>";
echo gettype(null) . "<br/>";

?>

<!--Following Program is done in Class -->

<?php

$myvalue = 20;

echo gettype($myvalue);

?>