<!--The is_string () function is used to find whether a variable is a string or not.

Syntax => is_string (variable_name)
-->

<?php

 if (is_string("126269"))
     echo "It is a string\n";
 else
     echo "It is not a string";

 ?>