<!-- 
The is_int () function is used to test whether the type of the specified variable is an integer or not. 

syntax => is_int(variable_name)
 -->
<?php
$apple = 24;
$orange = 12.6269;
$mango = 56;

if (is_int($apple)) {
    echo "$apple" . " is integer<br />";
}
else {
    echo "$apple" . " is not integer<br />";
}

if (is_int($orange)) {
    echo "$orange" . " is integer<br />";
}
else {
    echo "$orange" . " is not integer<br />";
}

if (is_int($mango)) {
    echo "$mango" . " is integer<br />";
}
else {
    echo "$mango" . " is not integer<br />";
}

?>