<!--
The is_null () function is used to test whether a variable is NULL or not.

Syntax => is_null (variable_name)
-->

<?php

$mustafiz = true;

if (is_null($mustafiz)) {
    echo "Mustafiz is NULL";
}
else {
    echo "Mustafiz is not NULL";
}

?>


