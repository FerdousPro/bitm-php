<!--
The unserialize() converts to actual data from serialized data.
unserialize() takes a single serialized variable and converts it back into a PHP value.

Syntax => unserialize(string1)
-->

<?php

$fruits_list = serialize(array('apple', 'orange', 'mango'));
echo  $fruits_list . "<br>";

$fruits_order = unserialize($fruits_list);
var_dump($fruits_order); // for showing the result of unserialize function

?>