<!-- 
The is_array() function is used to find whether a variable is an array or not.

syntax => is_array (variable_name)
 -->
<?php
$ferdous = array('a', 'b', 'c');
if (is_array($ferdous))
    echo "This is an array...";
else
    echo "This is not an array...";
?>
