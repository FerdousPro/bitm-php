<!--
The serialize() converts a storable representation of a value.
A serialize data means sequence of bits so that it can be stored in a file,
a memory buffer, or transmitted across a network connection link.

Syntax => serialize(value1)
-->

<?php

$fruits_list = serialize(array('apple', 'orange', 'mango'));
echo  $fruits_list . "<br />";

?>