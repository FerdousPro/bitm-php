<!--The var_dump() function is used to display structured information (type and value) about one or more variables.

Syntax => var_dump(variable1, variable2,variable3, . . .)
-->

<?php
$apple = 56;
$orange = "Testy";
$mango = 54.5;

echo var_dump($apple) . "<br />";
echo var_dump($orange) . "<br />";
echo var_dump($mango) . "<br />";

?>

<!--Another example -->

<?php

$ferdous = array('My', 'name', 'is', array('x', 'y', 1, 2));

var_dump($ferdous);

?>